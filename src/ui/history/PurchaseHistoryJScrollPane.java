package ui.history;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import commoditys.CommodityAbstract;

public class PurchaseHistoryJScrollPane extends JScrollPane {

	/* 改行コード */
	private static final String NEW_LINE = System.getProperty("line.separator");

	private final JTextArea purchaseHisotryJTextArea;

	public PurchaseHistoryJScrollPane() {
		this.purchaseHisotryJTextArea = new JTextArea();
		this.purchaseHisotryJTextArea.append("購入履歴" + NEW_LINE);
		this.purchaseHisotryJTextArea.setEnabled(false);
		this.setViewportView(this.purchaseHisotryJTextArea);
	}

	/* 購入履歴を追加 */
	public void addPurchaseHistory(CommodityAbstract commodity) {
		String hitsory = commodity.getName() + "  " + commodity.getPrice() + "円" + NEW_LINE;
		this.purchaseHisotryJTextArea.append(hitsory);
	}
}