package ui;

import java.awt.GridLayout;
import javax.swing.JFrame;
import commoditys.CommodityAbstract;
import ui.commodity.CommoditiesJPanel;
import ui.history.PurchaseHistoryJScrollPane;
import ui.payment.PaymentJPanel;

public class VendingMachineJFrame extends JFrame {

	private final CommoditiesJPanel          commoditiesJPanel;
	private final PaymentJPanel              paymentJPanel;
	private final PurchaseHistoryJScrollPane purchaseHistoryJScrollPane;

	public VendingMachineJFrame() {
		this.setTitle("VendingMachine");

		this.commoditiesJPanel          = new CommoditiesJPanel();
		this.paymentJPanel              = new PaymentJPanel();
		this.purchaseHistoryJScrollPane = new PurchaseHistoryJScrollPane();

		this.setLayout(new GridLayout(3, 1));
		this.add(this.commoditiesJPanel);
		this.add(this.paymentJPanel);
		this.add(this.purchaseHistoryJScrollPane);
		updateCommoditiesLamp();

		this.setSize(400, 700);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/* 商品の購入 */
	public boolean purchase(CommodityAbstract commodity) {
		boolean canPurchase = this.paymentJPanel.purchase(commodity);
		updateCommoditiesLamp();
		if (canPurchase) {
			this.purchaseHistoryJScrollPane.addPurchaseHistory(commodity);
		}
		return canPurchase;
	}

	/* 入金 */
	public boolean payment(int money) {
		boolean canPayment = this.paymentJPanel.payment(money);
		updateCommoditiesLamp();
		return canPayment;
	}

	/* 入金しているお金の払い戻し */
	public int payback() {
		int money = this.paymentJPanel.payback();
		updateCommoditiesLamp();
		return money;
	}

	/* 自動販売機の購入可能ランプの点灯の有無を更新 */
	private void updateCommoditiesLamp() {
		int money = this.paymentJPanel.getDeposit();
		this.commoditiesJPanel.updateCommoditiesLamp(money);
	}
}