package ui.commodity;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import ui.VendingMachineJFrame;
import commoditys.CommodityAbstract;

public class CommodityJButton extends JButton {

	private final CommodityAbstract commodity;
	private final Lamp              lamp;

	public CommodityJButton(CommodityAbstract commodity) {
		this.commodity        = commodity;
		String commodityName  = this.commodity.getName();
		String commodityPrice = String.valueOf(this.commodity.getPrice());
		
		this.lamp                   = new Lamp();
		JLabel commodityNameJLabel  = new JLabel(commodityName);  /* 商品名の表示 */
		JLabel commodityPriceJLabel = new JLabel(commodityPrice); /* 商品価格の表示 */

		/* 表示する文字列を中央揃えにする */
		commodityNameJLabel.setHorizontalAlignment(JLabel.CENTER);
		commodityPriceJLabel.setHorizontalAlignment(JLabel.CENTER);

		this.setLayout(new BorderLayout());
		this.add(commodityNameJLabel,  BorderLayout.NORTH);
		this.add(this.lamp,            BorderLayout.WEST);
		this.add(commodityPriceJLabel, BorderLayout.CENTER);
		this.addMouseListener(new PurchaseMouseAdapter(this));
		this.lamp.addMouseListener(new PurchaseMouseAdapter(this));
	}

	public CommodityAbstract getCommodity() {
		return this.commodity;
	}

	public boolean canPurchase() {
		return false;
	}

	public void turnOn() {
		this.lamp.turnOn();
	}

	public void turnOff() {
		this.lamp.turnOff();
	}

	private static class Lamp extends JRadioButton {
		private Lamp() {
			this.setEnabled(false);
		}

		public void turnOn() {
			this.setSelected(true);
		}

		public void turnOff() {
			this.setSelected(false);
		}
	}

	private static class PurchaseMouseAdapter extends MouseAdapter {

		private final CommodityJButton commodityJButton;

		private PurchaseMouseAdapter(CommodityJButton commodityJButton) {
			this.commodityJButton = commodityJButton;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			this.commodityJButton.requestFocus(true);
			VendingMachineJFrame vendingMachineJFrame = (VendingMachineJFrame)SwingUtilities.getWindowAncestor(this.commodityJButton);
			vendingMachineJFrame.purchase(commodityJButton.getCommodity());
		}
	}
}