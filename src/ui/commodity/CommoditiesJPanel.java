package ui.commodity;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JPanel;
import commoditys.Commodity;

public class CommoditiesJPanel extends JPanel {

	private final ArrayList<Commodity> commodities; /* 商品リスト */

	public CommoditiesJPanel() {
		this.commodities = new ArrayList<Commodity>();
		/* 商品リストに商品を追加 */
		this.commodities.add(new Commodity.Coffee());
		this.commodities.add(new Commodity.Coffee());
		this.commodities.add(new Commodity.Orange());
		this.commodities.add(new Commodity.Cola());
		this.commodities.add(new Commodity.Pepsi());
		this.commodities.add(new Commodity.PETTea());
		this.commodities.add(new Commodity.PETJuice());
		this.commodities.add(new Commodity.PETTea());
		this.commodities.add(new Commodity.Tea());
		this.commodities.add(new Commodity.Tea());

		this.setLayout(new GridLayout(2, 5));
		Iterator<Commodity> iterator = this.commodities.iterator();
		while (iterator.hasNext()) {
			/* 商品リスト分の購入ボタンを追加 */
			Commodity commodity = iterator.next();
			this.add(commodity.toCommodityJButtn());
		}
	}

	/* 自動販売機の購入可能ランプの点灯の有無を更新 */
	public void updateCommoditiesLamp(int money) {
		Iterator<Commodity> iterator = this.commodities.iterator();
		while (iterator.hasNext()) {
			Commodity commodity               = iterator.next();
			CommodityJButton commodityJButton = commodity.toCommodityJButtn();
			if (commodity.canPurchase(money)) {
				/* 購入可能ランプを点灯 */
				commodityJButton.turnOn();
				continue;
			}
			/* 購入可能ランプを消灯 */
			commodityJButton.turnOff();
		}
	}
}