package ui.payment;

import java.awt.GridLayout;
import javax.swing.JPanel;
import commoditys.CommodityAbstract;

public class PaymentJPanel extends JPanel {

	private final PurseJTextField    purseJTextField;
	private final PaymentJTextField  paymentJTextField;
	private final ChangeLeverJButton changeLeverButton;
	private final DepositJTextField  depositJTextField;

	public PaymentJPanel() {
		this.purseJTextField   = new PurseJTextField();
		this.paymentJTextField = new PaymentJTextField();
		this.changeLeverButton = new ChangeLeverJButton();
		this.depositJTextField = new DepositJTextField();

		this.setLayout(new GridLayout(2, 2));
		this.add(this.purseJTextField);
		this.add(this.changeLeverButton);
		this.add(this.paymentJTextField);
		this.add(this.depositJTextField);
	}

	/* 購入の処理の結果を各Componentに反映させ, 現在の残高から購入出来たかを返す */
	public boolean purchase(CommodityAbstract commodity) {
		int deposit         = this.depositJTextField.getMoney();
		boolean canPurchase = commodity.canPurchase(deposit);
		if (!canPurchase) {
			return false;
		}

		int price  = commodity.getPrice();
		deposit   -= price;
		this.depositJTextField.setMoney(deposit);
		return true;
	}

	/* 入金額を返す */
	public int getDeposit() {
		return this.depositJTextField.getMoney();
	}

	/* 支払いの処理の結果を各Componentに反映させ, 現在の残高から支払い可能出来たかを返す */
	public boolean payment(int money) {
		if (!canPayment(money)) {
			/* 支払い不可 */
			this.paymentJTextField.setMoney(0);
			return false;
		}

		/* 支払い可能 */
		int deposit  = this.depositJTextField.getMoney();
		int balance  = this.purseJTextField.getMoney();
		deposit     += money;
		balance     -= money;

		this.depositJTextField.setMoney(deposit);
		this.purseJTextField.setMoney(balance);
		this.paymentJTextField.setMoney(0);
		return true;
	}

	/* 返金の処理の結果を各Componentに反映させ, 返金額を返す */
	public int payback() {
		int deposit  = this.depositJTextField.getMoney();
		int money    = this.purseJTextField.getMoney();
		money       += deposit;

		/* 入金額分のお金を財布に戻し, 入金額をリセットする */
		this.purseJTextField.setMoney(money);
		this.depositJTextField.setMoney(0);
		return deposit;
	}

	/* 現在の残高からお金を支払えるかどうかを返す */
	public boolean canPayment(int money) {
		int balance        = this.purseJTextField.getMoney();
		boolean canPayment = (money <= balance);
		return canPayment;
	}
}