package ui.payment;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import javax.swing.SwingUtilities;
import ui.VendingMachineJFrame;

public class PaymentJTextField extends MoneyJTextField {

	PaymentJTextField() {
		super(0);
	}

	/* 入金処理 */
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		int money                           = this.getMoney();
		VendingMachineJFrame vendingMachine = (VendingMachineJFrame)SwingUtilities.getWindowAncestor(this);
		vendingMachine.payment(money);
		this.setMoney(0);
	}

	@Override
	public void focusLost(FocusEvent e) {
		super.focusLost(e);
		this.setMoney(0);
	}
}