package ui.payment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import ui.VendingMachineJFrame;

public class ChangeLeverJButton extends JButton implements ActionListener {

	ChangeLeverJButton() {
		super("Pay back");
		this.addActionListener(this);
	}

	/* 払い戻し処理 */
	@Override
	public void actionPerformed(ActionEvent e) {
		VendingMachineJFrame vendingMachineJFrame = (VendingMachineJFrame)SwingUtilities.getWindowAncestor(this);
		vendingMachineJFrame.payback();
	}
}