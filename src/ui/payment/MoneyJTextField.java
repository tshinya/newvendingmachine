package ui.payment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTextField;

public class MoneyJTextField extends JTextField implements FocusListener, ActionListener {

	private int money;

	MoneyJTextField(int money) {
		this.money = (0 <= money) ? money : 0;
		this.setText(String.valueOf(this.money));
		this.addActionListener(this);
		this.addFocusListener(this);
	}

	public final int getMoney() {
		return this.money;
	}

	public final void setMoney(int money) {
		this.money = (0 <= money) ? money : 0;
		this.setText(String.valueOf(this.money));
	}

	public final boolean isMoney() {
		try {
			int money       = Integer.parseInt(this.getText());
			boolean isMoney = (0 <= money);
			return isMoney;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (isMoney()) {
			this.money = Integer.valueOf(this.getText());
			return;
		}
		this.setText(String.valueOf(this.money));
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void focusLost(FocusEvent e) {
		if (isMoney()) {
			this.money = Integer.valueOf(this.getText());
			return;
		}
		this.setText(String.valueOf(this.money));
	}
}