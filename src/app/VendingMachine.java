package app;

import ui.VendingMachineJFrame;

public class VendingMachine {

	private VendingMachine() {
		new VendingMachineJFrame();
	}

	public static void main(String[] args) {
		new VendingMachine();
	}
}