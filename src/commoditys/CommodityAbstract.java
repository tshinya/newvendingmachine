package commoditys;

import ui.commodity.CommodityJButton;

public abstract class CommodityAbstract {

	private final String           commodityName;    /* 商品名 */
	private final int              commodityPrice;   /* 商品価格 */
	private final CommodityJButton commodityJButton; /* 商品購入ボタン */

	CommodityAbstract(String commodityName, int commodityPrice) {
		this.commodityName    = commodityName;
		this.commodityPrice   = commodityPrice;
		this.commodityJButton = new CommodityJButton(this);
	}

	/* 商品の価格を返す */
	public final int getPrice() {
		return this.commodityPrice;
	}

	/* 商品名を返す */
	public final String getName() {
		return this.commodityName;
	}

	/* 商品を購入可能かどうかを返す */
	public boolean canPurchase(int money) {
		boolean canPurchase = (this.commodityPrice <= money);
		return canPurchase;
	}

	/* この商品に対応する自動販売機の購入ボタンを返す */
	public CommodityJButton toCommodityJButtn() {
		return this.commodityJButton;
	}
}