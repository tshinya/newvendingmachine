package commoditys;

public class Commodity extends CommodityAbstract {

	private Commodity(String commodityName, int commodityPrice) {
		super(commodityName, commodityPrice);
	}

	/* 商品 */
	public static class Coffee extends Commodity {
		public Coffee() {
			super("Coffee", 110);
		}
	}

	public static class Orange extends Commodity {
		public Orange() {
			super("Orange", 110);
		}
	}

	public static class Cola extends Commodity {
		public Cola() {
			super("Cola", 110);
		}
	}

	public static class Pepsi extends Commodity {
		public Pepsi() {
			super("Pepsi", 110);
		}
	}

	public static class Tea extends Commodity {
		public Tea() {
			super("Tea", 110);
		}
	}

	public static class PETTea extends Commodity {
		public PETTea() {
			super("PET-Tea", 150);
		}
	}

	public static class PETJuice extends Commodity {
		public PETJuice() {
			super("PET-Juice", 150);
		}
	}
}